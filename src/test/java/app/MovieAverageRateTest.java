package app;

import app.repository.RateRepository;
import lombok.val;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.math.MathContext;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class MovieAverageRateTest {

    @Autowired
    RateRepository rateRepository;

    BigDecimal getAverage(BigDecimal a, BigDecimal b) {
        return a.add(b).divide(new BigDecimal(2), new MathContext(2));
    }

    @Test
    public void twoSameValues() {
        val a = new BigDecimal(2);
        val b = new BigDecimal(2);
        assertEquals(new BigDecimal(2), getAverage(a, b), "Regular multiplication should work");
    }

    @Test
    public void twoDifferentValues() {
        val a = new BigDecimal(5);
        val b = new BigDecimal(2);
        assertEquals(new BigDecimal("3.5"), getAverage(a, b), "Regular multiplication should work");
    }
}
