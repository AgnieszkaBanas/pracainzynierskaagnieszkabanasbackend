package app.repository;

import app.model.database.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface RateRepository extends JpaRepository<Rate, Long> {

    @Query("SELECT AVG(value) FROM Rate WHERE movie.id=:movieId")
    BigDecimal getAverageRate(@Param("movieId") Long movieId);

    List<Rate> findAllByMovieId(Long movieId);
}
