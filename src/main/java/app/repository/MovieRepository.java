package app.repository;

import app.model.database.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    Optional<Movie> findByTitleIgnoreCase(String title);

    List<Movie> findAllByCategory(String category);

    Page<Movie> findByTitleStartsWithIgnoreCase(String title, Pageable pageable);

    Page<Movie> findByCategoryAndTitleStartsWithIgnoreCase(String category, String title, Pageable pageable);

    Page<Movie> findByCategory(String category, Pageable pageable);

    Page<Movie> findByCategoryAndAverageRateNotNullOrderByAverageRateDesc(String category, Pageable pageable);

    Page<Movie> findByAddedById(Long addedById, Pageable pageable);

    Page<Movie> findAllByAverageRateNotNullOrderByAverageRateDesc(Pageable pageable);

    @Query(value = "SELECT title FROM movie join system_user_favourite_movies sufm on movie.id = sufm.movie_id " +
            "group by title ORDER BY COUNT(movie_id) DESC LIMIT 5", nativeQuery = true)
    List<String> findMostPopularMovies();

    @Query(value = "SELECT * FROM movie join system_user_favourite_movies sufm on movie.id = sufm.movie_id " +
            "WHERE sufm.user_id = :likedById", nativeQuery = true)
    Page<Movie> findFavouriteMovies(Long likedById, Pageable pageable);
}
