package app.repository;

import app.model.database.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmailIgnoreCase(String email);

    Page<User> findByIsReviewerIsTrueAndNameContainsIgnoreCase(String name, Pageable pageable);

    Page<User> findByIsReviewerIsTrue(Pageable paging);

    @Query(value = "SELECT name FROM system_user join user_following uf on system_user.id = uf.following_id " +
            "WHERE is_reviewer = true group by name ORDER BY COUNT(following_id) DESC LIMIT 5", nativeQuery = true)
    List<String> findMostPopularReviewers();

    Page<User> findByNameContainsIgnoreCase(String name, Pageable paging);

    @Query(value = "SELECT * FROM system_user join user_following on id = user_following.user_id WHERE following_id = :id", nativeQuery = true)
    Page<User> findFollowers(Long id, Pageable pageable);

    @Query(value = "SELECT * FROM system_user join user_following on id = user_following.following_id WHERE user_id = :id", nativeQuery = true)
    Page<User> findFollowing(Long id, Pageable pageable);

    @Query(value = "SELECT * FROM system_user su join rate on su.id = user_id WHERE movie_id = :movieId", nativeQuery = true)
    Page<User> findAllRatedBy(Long movieId, Pageable pageable);

    User findByFirstName(String firstName);
}
