package app.repository;

import app.model.database.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends PagingAndSortingRepository<Review, Long> {

    @Query(value = "SELECT * FROM review WHERE reviewed_by_id = :reviewedById", nativeQuery = true)
    Page<Review> findAllByReviewedById(Long reviewedById, Pageable pageable);

    @Query(value = "SELECT * FROM review JOIN movie m ON movie_id = m.id WHERE lower(m.title) LIKE lower(CONCAT(:movieTitle,'%'))", nativeQuery = true)
    Page<Review> findAllByMovieTitle(String movieTitle, Pageable pageable);

    @Query(value = "SELECT * FROM review JOIN system_user u ON reviewed_by_id = u.id WHERE lower(u.name) LIKE lower(CONCAT(:authorName,'%'))", nativeQuery = true)
    Page<Review> findAllByAuthorName(String authorName, Pageable pageable);

    @Query(value = "SELECT * FROM review " +
            "    JOIN system_user u " +
            "        ON reviewed_by_id = u.id " +
            "    JOIN movie m " +
            "        ON movie_id = m.id " +
            "WHERE lower(m.title) LIKE lower(CONCAT(:movieTitle,'%')) AND lower(u.name) LIKE lower(CONCAT(:authorName,'%'))", nativeQuery = true)
    Page<Review> findAllByMovieTitleAndAuthorName(String movieTitle, String authorName, Pageable pageable);
}
