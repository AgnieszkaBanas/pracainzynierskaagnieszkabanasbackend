package app.mapper;

import app.model.api.InitialUserDataRequest;
import app.model.api.UserDataResponse;
import app.model.api.UserDetailsResponse;
import app.model.database.User;
import app.service.ImageService;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "firstLogin", constant = "false")
    User updateUser(@MappingTarget User user, InitialUserDataRequest input);

    @Mapping(target = "followingCount", ignore = true)
    @Mapping(target = "followersCount", ignore = true)
    @Mapping(target = "likedMoviesCount", ignore = true)
    UserDetailsResponse userDetailsResponse(User user);

    @AfterMapping
    default void updateUserDetailsResponse(@MappingTarget UserDetailsResponse userDetailsResponse, User user) {
        userDetailsResponse.setFollowingCount(user.getFollowing().size());
        userDetailsResponse.setFollowersCount(user.getBeFollowing().size());
        userDetailsResponse.setLikedMoviesCount(user.getFavouriteMovies().size());
        userDetailsResponse.setImageBytes(ImageService.decompressImageBytes(user.getImageBytes()));
    }

    List<UserDataResponse> followers(List<User> users);

    UserDataResponse userDataResponse(User user);

    @AfterMapping
    default void updateUserDataResponse(@MappingTarget UserDataResponse userDataResponse, User user) {
        userDataResponse.setImageBytes(ImageService.decompressImageBytes(user.getImageBytes()));
    }
}
