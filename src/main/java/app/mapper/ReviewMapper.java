package app.mapper;

import app.model.api.ReviewResponse;
import app.model.api.UserReviewData;
import app.model.database.Review;
import app.service.ImageService;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ReviewMapper {

    @Mapping(target = "movieTitle", source = "movie.title")
    UserReviewData userReviewData(Review review);

    @Mapping(target = "imageBytes", ignore = true)
    @Mapping(target = "movieTitle", source = "movie.title")
    ReviewResponse reviewResponse(Review review);

    @AfterMapping
    default void updateReviewResponse(@MappingTarget ReviewResponse reviewResponse, Review review) {
        reviewResponse.setImageBytes(ImageService.decompressImageBytes(review.getMovie().getImageBytes()));
    }
}
