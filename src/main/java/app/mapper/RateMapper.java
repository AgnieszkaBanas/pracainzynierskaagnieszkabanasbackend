package app.mapper;

import app.model.api.UserMovieRate;
import app.model.database.Rate;
import app.service.ImageService;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RateMapper {

    List<UserMovieRate> usersMovieRate(List<Rate> rates);

    @Mapping(target = "imageBytes", source = "rate.user.imageBytes")
    @Mapping(target = "name", source = "rate.user.name")
    @Mapping(target = "id", source = "rate.user.id")
    UserMovieRate userMovieRate(Rate rate);

    @AfterMapping
    default void updateUserMovieRate(@MappingTarget UserMovieRate userMovieRate) {
        userMovieRate.setImageBytes(ImageService.decompressImageBytes(userMovieRate.getImageBytes()));
    }
}
