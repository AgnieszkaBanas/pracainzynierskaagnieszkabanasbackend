package app.mapper;

import app.model.api.MovieBasicData;
import app.model.api.MovieData;
import app.model.api.MovieDetailsResponse;
import app.model.database.Movie;
import app.service.ImageService;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MovieMapper {

    MovieBasicData movieBasicData(Movie movie);

    @AfterMapping
    default void updateMovieBasicData(@MappingTarget MovieBasicData movieBasicData, Movie movie) {
        movieBasicData.setImageBytes(ImageService.decompressImageBytes(movie.getImageBytes()));
    }

    List<MovieData> moviesData(List<Movie> movie);

    MovieData movieData(Movie movie);

    @Mapping(target = "addedBy", source = "movie.addedBy.name")
    MovieDetailsResponse movieDetailsResponse(Movie movie, Long userId);

    @AfterMapping
    default void updateMovieDetails(@MappingTarget MovieDetailsResponse movieDetailsResponse, Movie movie, Long userId) {
        movieDetailsResponse.setImageBytes(ImageService.decompressImageBytes(movie.getImageBytes()));
        movieDetailsResponse.setLikedByCurrentUser(movie.getUsers().stream().anyMatch(user -> user.getId().equals(userId)));
        movieDetailsResponse.setRatedByCurrentUser(movie.getRates().stream().anyMatch(rate -> rate.getUser().getId().equals(userId)));
    }
}
