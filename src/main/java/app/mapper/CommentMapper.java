package app.mapper;

import app.model.api.ReviewComment;
import app.model.database.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CommentMapper {

    List<ReviewComment> reviewComments(List<Comment> comments);

    @Mapping(target = "author", source = "author.firstName")
    ReviewComment reviewComment(Comment comment);
}
