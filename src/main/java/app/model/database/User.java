package app.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Table(name = "system_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String name;

    @Column(length = 2000)
    private String description;
    private String countryWithCity;
    private LocalDate accountCreationDate;

    @Column(unique = true)
    private String email;
    private String password;
    private Boolean firstLogin;
    private Boolean isReviewer;
    private byte[] imageBytes;

    @ManyToMany()
    @JsonIgnore
    @JoinTable(name = "user_following",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "following_id", referencedColumnName = "id"))
    protected List<User> following;

    @ManyToMany(mappedBy = "following")
    protected List<User> beFollowing;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"))
    private List<Movie> favouriteMovies = new ArrayList<>();

//    @JsonIgnore
//    @ManyToMany
//    @JoinTable(
//            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"))
//    private List<Movie> ratedMovies = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "addedBy", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Movie> addedMovies = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "reviewedBy", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Review> addedReviews = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Rate> rates = new ArrayList<>();
}
