package app.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;

    @Column(length = 2000)
    private String description;
    private String director;
    private LocalDate releaseDate;
    private BigDecimal averageRate;
    private String category;
    private byte[] imageBytes;

    @JsonIgnore
    @ManyToMany(mappedBy = "favouriteMovies")
    private List<User> users = new ArrayList<>();

    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "added_by_id", referencedColumnName = "id")
    private User addedBy;

    @JsonIgnore
    @OneToMany(mappedBy="movie", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Review> reviews = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy="movie", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Rate> rates = new ArrayList<>();
}
