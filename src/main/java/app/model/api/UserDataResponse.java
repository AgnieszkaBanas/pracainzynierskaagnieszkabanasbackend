package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDataResponse {
    private Long id;
    private String name;
    private byte[] imageBytes;
    private Boolean isReviewer;
}
