package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailsResponse {
    private Long id;
    private String name;
    private String description;
    private String countryWithCity;
    private LocalDate accountCreationDate;
    private List<String> categories;
    private byte[] imageBytes;
    private Integer followingCount;
    private Integer followersCount;
    private Integer likedMoviesCount;
    private Boolean isReviewer;
}
