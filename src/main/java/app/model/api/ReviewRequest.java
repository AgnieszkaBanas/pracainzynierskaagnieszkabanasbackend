package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewRequest {

    @NotEmpty
    private String title;

    @NotNull
    private Long userId;

    @NotNull
    private Long movieId;

    @NotEmpty
    private String content;
}
