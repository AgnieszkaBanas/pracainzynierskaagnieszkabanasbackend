package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieDetailsResponse {
    private Long id;
    private String title;
    private String category;
    private String director;
    private LocalDate releaseDate;
    private String description;
    private BigDecimal averageRate;
    private String addedBy;
    private String ratedBy;
    private BigDecimal placeInGeneralRanking;
    private BigDecimal placeInCategoryRanking;
    private Boolean ratedByCurrentUser;
    private byte[] imageBytes;
    private Boolean likedByCurrentUser;
}
