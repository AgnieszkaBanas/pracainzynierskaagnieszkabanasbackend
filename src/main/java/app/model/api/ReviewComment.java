package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewComment {
    private Long authorId;
    private String content;
    private String author;
    private String creationDate;
}
