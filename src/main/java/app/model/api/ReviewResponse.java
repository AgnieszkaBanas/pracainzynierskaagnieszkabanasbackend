package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewResponse {
    private Long id;
    private String title;
    private String content;
    private LocalDate creationDate;
    private String movieTitle;
    private ReviewedUser reviewedBy;
    private byte[] imageBytes;
}
