package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieRequest {

    @NotEmpty
    private String title;

    @NotNull
    private Long userId;

    @NotNull
    private String category;

    @NotEmpty
    private String description;

    @NotEmpty
    private String director;

    private LocalDate releaseDate;
}
