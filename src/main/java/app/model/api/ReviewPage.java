package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReviewPage {
    private List<ReviewResponse> reviews;
    private int currentPage;
    private long totalItems;
    private int totalPages;
}
