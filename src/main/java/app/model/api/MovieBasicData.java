package app.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieBasicData {
    private Long id;
    private String title;
    private byte[] imageBytes;
    private BigDecimal averageRate;
    private String category;
}
