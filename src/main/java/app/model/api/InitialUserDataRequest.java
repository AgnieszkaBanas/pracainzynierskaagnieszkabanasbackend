package app.model.api;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InitialUserDataRequest {
    private Long id;
    private String description;
    private String countryWithCity;
    private List<Long> categoryIds;
}
