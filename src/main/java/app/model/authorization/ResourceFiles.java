package app.model.authorization;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ResourceFiles {

    @Value("classpath:app/model/file/no_image.png")
    private Resource noImage;

    @Value("classpath:app/model/file/no_profile_image.png")
    private Resource noProfileImage;

    @Bean(name = "noImage")
    public String noImage() {
        try (InputStream is = noImage.getInputStream()) {
            return StreamUtils.copyToString(is, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Bean(name = "noProfileImage")
    public String noProfileImage() {
        try (InputStream is = noProfileImage.getInputStream()) {
            return StreamUtils.copyToString(is, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
