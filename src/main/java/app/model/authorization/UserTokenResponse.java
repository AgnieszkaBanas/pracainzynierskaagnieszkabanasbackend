package app.model.authorization;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

@Value
@Builder
public class UserTokenResponse implements Serializable {
    String token;
    Long id;
}
