package app.controller;

import app.model.api.*;
import app.model.authorization.UserTokenResponse;
import app.service.MovieService;
import app.service.ReviewService;
import app.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@AllArgsConstructor
public class UserController {
    private final UserService userService;
    private final MovieService movieService;
    private final ReviewService reviewService;

    @PostMapping("/users")
    public ResponseEntity<Void> register(@RequestBody @Valid UserRegistrationRequest userRegistrationRequest) {
        userService.register(userRegistrationRequest);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/users/email-validation")
    public ResponseEntity<String> validateEmail(@RequestParam String email) {
        if (userService.checkIfEmailIsUnique(email)) {
            return ResponseEntity.badRequest().body("Email already exists");
        }
        return ResponseEntity.ok().body(null);
    }

    @PostMapping("/users/login")
    public ResponseEntity<UserTokenResponse> loginUser(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok().body(userService.loginUser(loginRequest));
    }

    @PostMapping("/users/initial-data")
    public ResponseEntity<Void> addInitialUserData(@RequestPart InitialUserDataRequest initialUserDataRequest,
                                                   @RequestPart("image") MultipartFile file) throws IOException {
        userService.addInitialUserData(initialUserDataRequest, file);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserDetailsResponse> getUserDetails(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(userService.getUserDetails(id));
    }

    @GetMapping("/users/{id}/dashboard-data")
    public ResponseEntity<UserDataResponse> getUserDashboardData(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(userService.getUserDashboardData(id));
    }

    @GetMapping("/users/{id}/movies")
    public ResponseEntity<MoviesAddedByUserResponse> getUserMovies(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(movieService.getMoviesAddedByUser(id));
    }

    @GetMapping("/users/{id}/reviews")
    public ResponseEntity<UserReviewsResponse> getUserReviews(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(reviewService.getUserReviews(id));
    }

    @GetMapping("/users/{id}/first-login")
    public ResponseEntity<Boolean> checkFirstLogin(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(userService.getFirstLogin(id));
    }

    @PostMapping("/users/{id}/followers/{followingId}")
    public ResponseEntity<String> addToFollowers(@PathVariable @NotNull Long id,
                                                 @PathVariable @NotNull Long followingId) {
        userService.addToFollowing(id, followingId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/users/{id}/followers")
    public ResponseEntity<FollowersResponse> getUserFollowers(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(userService.getFollowingAndFollowers(id));
    }

    @GetMapping("/users/list")
    public ResponseEntity<UserPage> getUsersList(@RequestParam(required = false) String userName,
                                                  @RequestParam(required = false) Long movieId,
                                                  @RequestParam(required = false) Long followersUserId,
                                                  @RequestParam(required = false) Long followingUserId,
                                                  @RequestParam(required = false) Boolean reviewers,
                                                  @RequestParam(defaultValue = "0") int page,
                                                  @RequestParam(defaultValue = "3") int size) {
        return ResponseEntity.ok().body(userService.getUserPage(userName, movieId, followersUserId, followingUserId,
                reviewers, page, size));
    }

    @PostMapping("/users/{id}/favourite-movies/{movieId}")
    public ResponseEntity<String> getUserFollowers(@PathVariable @NotNull Long id,
                                                   @PathVariable @NotNull Long movieId) {
        userService.addToFavouriteMovies(id, movieId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/users/{id}/favourite-movies")
    public ResponseEntity<MoviesResponse> getUserFavouriteMovies(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(movieService.getFavouriteMovies(id));
    }

    @GetMapping("/users/countries-with-cities")
    public ResponseEntity<CountriesWithCitiesResponse> getCountriesWithCities() throws IOException {
        return ResponseEntity.ok().body(userService.getCountriesWithCities());
    }
}
