package app.controller;

import app.model.api.*;
import app.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@AllArgsConstructor
public class ReviewController {
    private final ReviewService reviewService;

    @PostMapping("/reviews")
    public ResponseEntity<String> addReview(@RequestBody @Valid ReviewRequest reviewRequest) {
        reviewService.addReview(reviewRequest);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/reviews/{id}")
    public ResponseEntity<ReviewResponse> getReviewDetails(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(reviewService.getReviewDetails(id));
    }

    @GetMapping("/reviews/list")
    public ResponseEntity<ReviewPage> getReviewsList(@RequestParam(required = false) String movieTitle,
                                                     @RequestParam(required = false) String authorName,
                                                     @RequestParam(required = false) Long reviewedById,
                                                     @RequestParam(defaultValue = "0") int page,
                                                     @RequestParam(defaultValue = "3") int size) {
        return ResponseEntity.ok().body(reviewService.getReviewPage(movieTitle, authorName, reviewedById, page, size));
    }

    @GetMapping("/reviews/{id}/comments")
    public ResponseEntity<ReviewCommentsResponse> getReviewComments(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(reviewService.getReviewComments(id));
    }

    @PostMapping("/reviews/{id}/comments")
    public ResponseEntity<String> addComment(@RequestBody @Valid CommentRequest commentRequest,
                                             @PathVariable @NotNull Long id) {
        reviewService.addComment(commentRequest, id);
        return ResponseEntity.ok().build();
    }
}
