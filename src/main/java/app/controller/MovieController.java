package app.controller;

import app.model.api.*;
import app.service.MovieService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@AllArgsConstructor
public class MovieController {
    private final MovieService movieService;

    @PostMapping("/movies")
    public ResponseEntity<String> addMovie(@RequestPart("movieRequest") @Valid MovieRequest movieRequest,
                                           @RequestPart("image") MultipartFile file) throws IOException {
        movieService.addMovie(movieRequest, file);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/movies")
    public ResponseEntity<MoviesResponse> getMoviesByCategory(@RequestParam @NotNull String category) {
        return ResponseEntity.ok().body(movieService.getMoviesByCategory(category));
    }

    @GetMapping("/movies/movie-validation")
    public ResponseEntity<String> validateMovie(@RequestParam String title) {
        if (movieService.checkIfMovieIsUnique(title)) {
            return ResponseEntity.badRequest().body("Movie already exists");
        }
        return ResponseEntity.ok().body(null);
    }

    @GetMapping("/movies/{id}/current-user/{userId}")
    public ResponseEntity<MovieDetailsResponse> getMovieDetails(@PathVariable @NotNull Long id,
                                                                @PathVariable @NotNull Long userId) {
        return ResponseEntity.ok().body(movieService.getMovieDetails(id, userId));
    }

    @PostMapping("/movies/{id}/rates")
    public ResponseEntity<String> rateMovie(@PathVariable @NotNull Long id,
                                            @RequestBody @Valid RateMovieRequest rateMovieRequest) {
        movieService.rateMovie(rateMovieRequest, id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/movies/{id}/users")
    public ResponseEntity<UsersMovieRateResponse> getRatedUsers(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok().body(movieService.getUsersMovieRate(id));
    }

    @GetMapping("/movies/list")
    public ResponseEntity<MoviePage> getMoviesList(@RequestParam(required = false) String movie,
                                                   @RequestParam(required = false) String category,
                                                   @RequestParam(required = false) Long likedById,
                                                   @RequestParam(required = false) Long addedById,
                                                   @RequestParam(defaultValue = "0") int page,
                                                   @RequestParam(defaultValue = "3") int size) {
        return ResponseEntity.ok().body(movieService.getMoviePage(movie, category, likedById, addedById, page, size));
    }

    @GetMapping("/movies/ranking")
    public ResponseEntity<MoviePage> getMovieRanking(@RequestParam(required = false) String category,
                                                     @RequestParam(defaultValue = "0") int page,
                                                     @RequestParam(defaultValue = "3") int size) {
        return ResponseEntity.ok().body(movieService.getMovieRankingPage(category, page, size));
    }

    @GetMapping("/movies/main-data")
    public ResponseEntity<MainDataResponse> getMainData() {
        return ResponseEntity.ok().body(movieService.getMainData());
    }

    @GetMapping("/movies/categories")
    public ResponseEntity<CategoriesResponse> getAllMoviesCategories() throws IOException {
        return ResponseEntity.ok().body(movieService.getAllCategories());
    }
}
