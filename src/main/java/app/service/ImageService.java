package app.service;

import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Service
@AllArgsConstructor
public class ImageService {
    public static byte[] compressImageBytes(byte[] data) {
        val deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        val byteArrayOutputStream = new ByteArrayOutputStream(data.length);
        val buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            byteArrayOutputStream.write(buffer, 0, count);
        }
        try {
            byteArrayOutputStream.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] decompressImageBytes(byte[] data) {
        val inflater = new Inflater();
        inflater.setInput(data);
        val byteArrayOutputStream = new ByteArrayOutputStream(data.length);
        val buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                byteArrayOutputStream.write(buffer, 0, count);
            }
            byteArrayOutputStream.close();
        } catch (IOException | DataFormatException e) {
            System.out.println(e.getMessage());
        }
        return byteArrayOutputStream.toByteArray();
    }
}
