package app.service;

import app.mapper.CommentMapper;
import app.mapper.ReviewMapper;
import app.model.api.*;
import app.model.database.Comment;
import app.model.database.Review;
import app.repository.CommentRepository;
import app.repository.MovieRepository;
import app.repository.ReviewRepository;
import app.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ReviewService {
    private final ReviewRepository reviewRepository;
    private final MovieRepository movieRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final ReviewMapper reviewMapper;
    private final CommentMapper commentMapper;
    private final static long REVIEWS_ADDED_BY_USER_COUNT = 4;

    public void addReview(ReviewRequest reviewRequest) {
        val user = userRepository.findById(reviewRequest.getUserId()).orElseThrow();
        user.setIsReviewer(true);
        val review = Review
                .builder()
                .title(reviewRequest.getTitle())
                .content(reviewRequest.getContent())
                .creationDate(java.time.LocalDate.now())
                .reviewedBy(user)
                .movie(movieRepository.findById(reviewRequest.getMovieId()).orElseThrow())
                .build();
        reviewRepository.save(review);
        userRepository.save(user);
    }

    @Transactional
    public UserReviewsResponse getUserReviews(Long userId) {
        return UserReviewsResponse
                .builder()
                .reviews(userRepository.findById(userId).orElseThrow().getAddedReviews()
                        .stream()
                        .limit(REVIEWS_ADDED_BY_USER_COUNT)
                        .map(reviewMapper::userReviewData)
                        .collect(Collectors.toList()))
                .build();
    }

    public ReviewResponse getReviewDetails(Long id) {
        return reviewMapper.reviewResponse(reviewRepository.findById(id).orElseThrow());
    }

    @Transactional
    public ReviewPage getReviewPage(String movieTitle, String authorName, Long reviewedById, int page, int size) {
        List<Review> reviews;
        Pageable paging = PageRequest.of(page, size);

        Page<Review> pageReviews;
        if (!movieTitle.isEmpty() && authorName.isEmpty()) {
            pageReviews = reviewRepository.findAllByMovieTitle(movieTitle, paging);
        } else if (!movieTitle.isEmpty()) {
            pageReviews = reviewRepository.findAllByMovieTitleAndAuthorName(movieTitle, authorName, paging);
        } else if (!authorName.isEmpty()) {
            pageReviews = reviewRepository.findAllByAuthorName(authorName, paging);
        } else if (reviewedById != null) {
            pageReviews = reviewRepository.findAllByReviewedById(reviewedById, paging);
        } else {
            pageReviews = reviewRepository.findAll(paging);
        }

        reviews = pageReviews.getContent();

        return ReviewPage
                .builder()
                .reviews(reviews.stream().map(reviewMapper::reviewResponse).collect(Collectors.toList()))
                .currentPage(pageReviews.getNumber())
                .totalItems(pageReviews.getTotalElements())
                .totalPages(pageReviews.getTotalPages())
                .build();
    }

    public ReviewCommentsResponse getReviewComments(Long reviewId) {
        return ReviewCommentsResponse.builder()
                .comments(commentMapper.reviewComments(reviewRepository.findById(reviewId).orElseThrow().getComments()))
                .build();
    }

    public void addComment(CommentRequest commentRequest, Long id) {
        commentRepository.save(Comment.builder()
                .author(userRepository.findById(commentRequest.getUserId()).orElseThrow())
                .review(reviewRepository.findById(id).orElseThrow())
                .creationDate(java.time.LocalDate.now())
                .content(commentRequest.getContent())
                .build());
    }
}
