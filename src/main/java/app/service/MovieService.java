package app.service;

import app.mapper.MovieMapper;
import app.mapper.RateMapper;
import app.model.api.*;
import app.model.database.Movie;
import app.model.database.Rate;
import app.repository.MovieRepository;
import app.repository.RateRepository;
import app.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MovieService {
    private final MovieRepository movieRepository;
    private final UserRepository userRepository;
    private final MovieMapper movieMapper;
    private final RateRepository rateRepository;
    private final RateMapper rateMapper;
    private final static long MOVIES_ADDED_BY_USER_COUNT = 7;
    private final static long USER_FAVOURITE_MOVIES_COUNT = 7;

    public void addMovie(MovieRequest movieRequest, MultipartFile file) throws IOException {
        val movie = Movie
                .builder()
                .title(movieRequest.getTitle())
                .description(movieRequest.getDescription())
                .director(movieRequest.getDirector())
                .releaseDate(movieRequest.getReleaseDate())
                .addedBy(userRepository.findById(movieRequest.getUserId()).orElseThrow())
                .category(movieRequest.getCategory())
                .build();

        if (!file.isEmpty()) {
            movie.setImageBytes(ImageService.compressImageBytes(file.getBytes()));
        } else {
            movie.setImageBytes(ImageService.compressImageBytes(Files.readAllBytes(Paths.get("src/main/java/app/model/file/no_image.png"))));
        }
        movieRepository.save(movie);
    }

    public MoviesAddedByUserResponse getMoviesAddedByUser(Long userId) {
        return MoviesAddedByUserResponse
                .builder()
                .movies(userRepository.findById(userId).orElseThrow().getAddedMovies()
                        .stream()
                        .limit(MOVIES_ADDED_BY_USER_COUNT)
                        .map(movieMapper::movieBasicData)
                        .collect(Collectors.toList()))
                .build();
    }

    public MoviesResponse getMoviesByCategory(String category) {
        return MoviesResponse
                .builder()
                .movies(movieRepository.findAllByCategory(category)
                        .stream()
                        .map(movieMapper::movieData)
                        .collect(Collectors.toList()))
                .build();
    }

    public void rateMovie(RateMovieRequest rateMovieRequest, Long id) {
        val movie = movieRepository.findById(id).orElseThrow();
        rateRepository.save(Rate.builder()
                .value(rateMovieRequest.getRate())
                .movie(movie)
                .user(userRepository.findById(rateMovieRequest.getUserId()).orElseThrow())
                .build());
        movie.setAverageRate(rateRepository.getAverageRate(movie.getId()));
        movieRepository.save(movie);
    }

    public MovieDetailsResponse getMovieDetails(Long id, Long userId) {
        return movieMapper.movieDetailsResponse(movieRepository.findById(id).orElseThrow(), userId);
    }

    public UsersMovieRateResponse getUsersMovieRate(Long id) {
        val rates = rateRepository.findAllByMovieId(id);
        return UsersMovieRateResponse
                .builder()
                .users(rateMapper.usersMovieRate(rates))
                .build();
    }

    public MoviePage getMoviePage(String movieName, String category, Long likedById, Long addedById, int page, int size) {
        List<Movie> movies;
        Pageable paging = PageRequest.of(page, size);

        Page<Movie> pageMovies;
        if (likedById != null) {
            pageMovies = movieRepository.findFavouriteMovies(likedById, paging);
        } else if (addedById != null) {
            pageMovies = movieRepository.findByAddedById(addedById, paging);
        } else if (!movieName.isEmpty() && (!category.isEmpty())) {
            pageMovies = movieRepository.findByCategoryAndTitleStartsWithIgnoreCase(category, movieName, paging);
        } else if (!movieName.isEmpty()) {
            pageMovies = movieRepository.findByTitleStartsWithIgnoreCase(movieName, paging);
        } else if (!category.isEmpty()) {
            pageMovies = movieRepository.findByCategory(category, paging);
        } else {
            pageMovies = movieRepository.findAll(paging);
        }

        movies = pageMovies.getContent();

        return MoviePage
                .builder()
                .movies(movies.stream().map(movieMapper::movieBasicData).collect(Collectors.toList()))
                .currentPage(pageMovies.getNumber())
                .totalItems(pageMovies.getTotalElements())
                .totalPages(pageMovies.getTotalPages())
                .build();
    }

    public MoviePage getMovieRankingPage(String category, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Movie> moviesPage;

        if (!category.isEmpty()) {
            moviesPage = movieRepository.findByCategoryAndAverageRateNotNullOrderByAverageRateDesc(category, pageable);
        } else {
            moviesPage = movieRepository.findAllByAverageRateNotNullOrderByAverageRateDesc(pageable);
        }

        return MoviePage
                .builder()
                .movies(moviesPage.getContent().stream().map(movieMapper::movieBasicData).collect(Collectors.toList()))
                .currentPage(moviesPage.getNumber())
                .totalItems(moviesPage.getTotalElements())
                .totalPages(moviesPage.getTotalPages())
                .build();
    }

    public MainDataResponse getMainData() {
        return MainDataResponse
                .builder()
                .mostPopularReviewers(userRepository.findMostPopularReviewers())
                .mostPopularMovies(movieRepository.findMostPopularMovies())
                .build();
    }

    public boolean checkIfMovieIsUnique(String title) {
        return movieRepository.findByTitleIgnoreCase(title).isPresent();
    }

    public CategoriesResponse getAllCategories() throws IOException {
        return CategoriesResponse.builder()
                .categories(Arrays.asList(new ObjectMapper()
                        .readValue(Paths.get("src/main/java/app/model/file/categories.json")
                                .toFile(), String[].class)))
                .build();
    }

    public MoviesResponse getFavouriteMovies(Long userId) {
        val user = userRepository.findById(userId).orElseThrow();
        return MoviesResponse
                .builder()
                .movies(movieMapper.moviesData(user.getFavouriteMovies()
                        .stream()
                        .limit(USER_FAVOURITE_MOVIES_COUNT)
                        .collect(Collectors.toList())))
                .build();
    }
}
