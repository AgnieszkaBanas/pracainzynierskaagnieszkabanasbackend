package app.service;

import app.mapper.UserMapper;
import app.model.api.*;
import app.model.authorization.JwtTokenUtil;
import app.model.authorization.UserTokenResponse;
import app.model.database.User;
import app.repository.MovieRepository;
import app.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserMapper userMapper;
    private final MovieRepository movieRepository;

    private final static long USER_FOLLOWERS_COUNT = 7;
    private final static long USER_FOLLOWING_COUNT = 7;

    public void register(UserRegistrationRequest userRegistrationRequest) {
        userRepository.save(User.builder()
                .firstName(userRegistrationRequest.getFirstName())
                .lastName(userRegistrationRequest.getLastName())
                .name(userRegistrationRequest.getFirstName() + " " + userRegistrationRequest.getLastName())
                .email(userRegistrationRequest.getEmail())
                .password(encoder.encode(userRegistrationRequest.getPassword()))
                .accountCreationDate(java.time.LocalDate.now())
                .firstLogin(true)
                .isReviewer(false)
                .build());
    }

    public boolean checkIfEmailIsUnique(String email) {
        return userRepository.findByEmailIgnoreCase(email).isPresent();
    }

    public UserTokenResponse loginUser(LoginRequest loginRequest) {
        val authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        val user = (UserDetailsImplementation) authentication.getPrincipal();

        return UserTokenResponse
                .builder()
                .id(user.getId())
                .token(jwtTokenUtil.generateJwtToken(authentication))
                .build();
    }

    public void addInitialUserData(InitialUserDataRequest initialUserDataRequest, MultipartFile file) throws IOException {
        val user = userRepository.findById(initialUserDataRequest.getId()).orElseThrow();
        if (!file.isEmpty()) {
            user.setImageBytes(ImageService.compressImageBytes(file.getBytes()));
        } else {
            user.setImageBytes(ImageService.compressImageBytes(Files.readAllBytes(Paths.get("src/main/java/app/model/file/no_image.png"))));
        }
        userRepository.save(userMapper
                .updateUser(user, initialUserDataRequest));
    }

    public UserDetailsResponse getUserDetails(Long userId) {
        return userMapper.userDetailsResponse(userRepository.findById(userId).orElseThrow());
    }

    public UserDataResponse getUserDashboardData(Long userId) {
        return userMapper.userDataResponse(userRepository.findById(userId).orElseThrow());
    }

    public Boolean getFirstLogin(Long userId) {
        return userRepository.findById(userId).orElseThrow().getFirstLogin();
    }

    public void addToFollowing(Long id, Long followingId) {
        val user = userRepository.findById(id).orElseThrow();
        user.getFollowing().add(
                userRepository.findById(followingId).orElseThrow());
        userRepository.save(user);
    }

    public FollowersResponse getFollowingAndFollowers(Long userId) {
        val user = userRepository.findById(userId).orElseThrow();
        return FollowersResponse
                .builder()
                .followers(userMapper.followers(user.getBeFollowing())
                        .stream()
                        .limit(USER_FOLLOWING_COUNT)
                        .collect(Collectors.toList()))
                .following(userMapper.followers(user.getFollowing())
                        .stream()
                        .limit(USER_FOLLOWERS_COUNT)
                        .collect(Collectors.toList()))
                .build();
    }

    public UserPage getUserPage(String userName, Long movieId, Long followersUserId, Long followingUserId, Boolean reviewers,
                                int page, int size) {

        Pageable pageable = PageRequest.of(page, size);
        Page<User> pageUsers;
        if (!userName.isEmpty() && reviewers.equals(true)) {
            pageUsers = userRepository.findByIsReviewerIsTrueAndNameContainsIgnoreCase(userName, pageable);
        } else if (!userName.isEmpty()) {
            pageUsers = userRepository.findByNameContainsIgnoreCase(userName, pageable);
        } else if (followersUserId != null) {
            pageUsers = userRepository.findFollowers(followersUserId, pageable);
        } else if (followingUserId != null) {
            pageUsers = userRepository.findFollowing(followingUserId, pageable);
        } else if (movieId != null) {
            pageUsers = userRepository.findAllRatedBy(movieId, pageable);
        } else if (reviewers.equals(true)) {
            pageUsers = userRepository.findByIsReviewerIsTrue(pageable);
        } else {
            pageUsers = userRepository.findAll(pageable);
        }

        return UserPage
                .builder()
                .users(pageUsers.getContent().stream().map(userMapper::userDataResponse).collect(Collectors.toList()))
                .currentPage(pageUsers.getNumber())
                .totalItems(pageUsers.getTotalElements())
                .totalPages(pageUsers.getTotalPages())
                .build();
    }

    public void addToFavouriteMovies(Long userId, Long movieId) {
        val user = userRepository.findById(userId).orElseThrow();
        user.getFavouriteMovies().add(movieRepository.findById(movieId).orElseThrow());
        userRepository.save(user);
    }

    public CountriesWithCitiesResponse getCountriesWithCities() throws IOException {
        return CountriesWithCitiesResponse.builder()
                .countriesWithCities(Arrays.asList(new ObjectMapper()
                        .readValue(Paths.get("src/main/java/app/model/file/countries.json")
                                .toFile(), CountryWithCities[].class)))
                .build();
    }
}
