package app.mapper;

import app.model.api.MovieBasicData;
import app.model.api.MovieData;
import app.model.api.MovieDetailsResponse;
import app.model.database.Movie;
import app.model.database.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-28T13:44:59+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.12 (Amazon.com Inc.)"
)
@Component
public class MovieMapperImpl implements MovieMapper {

    @Override
    public MovieBasicData movieBasicData(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieBasicData movieBasicData = new MovieBasicData();

        movieBasicData.setId( movie.getId() );
        movieBasicData.setTitle( movie.getTitle() );
        byte[] imageBytes = movie.getImageBytes();
        if ( imageBytes != null ) {
            movieBasicData.setImageBytes( Arrays.copyOf( imageBytes, imageBytes.length ) );
        }
        movieBasicData.setAverageRate( movie.getAverageRate() );
        movieBasicData.setCategory( movie.getCategory() );

        updateMovieBasicData( movieBasicData, movie );

        return movieBasicData;
    }

    @Override
    public List<MovieData> moviesData(List<Movie> movie) {
        if ( movie == null ) {
            return null;
        }

        List<MovieData> list = new ArrayList<MovieData>( movie.size() );
        for ( Movie movie1 : movie ) {
            list.add( movieData( movie1 ) );
        }

        return list;
    }

    @Override
    public MovieData movieData(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieData movieData = new MovieData();

        movieData.setId( movie.getId() );
        movieData.setTitle( movie.getTitle() );
        movieData.setCategory( movie.getCategory() );

        return movieData;
    }

    @Override
    public MovieDetailsResponse movieDetailsResponse(Movie movie, Long userId) {
        if ( movie == null && userId == null ) {
            return null;
        }

        MovieDetailsResponse movieDetailsResponse = new MovieDetailsResponse();

        if ( movie != null ) {
            movieDetailsResponse.setAddedBy( movieAddedByName( movie ) );
            movieDetailsResponse.setId( movie.getId() );
            movieDetailsResponse.setTitle( movie.getTitle() );
            movieDetailsResponse.setCategory( movie.getCategory() );
            movieDetailsResponse.setDirector( movie.getDirector() );
            movieDetailsResponse.setReleaseDate( movie.getReleaseDate() );
            movieDetailsResponse.setDescription( movie.getDescription() );
            movieDetailsResponse.setAverageRate( movie.getAverageRate() );
            byte[] imageBytes = movie.getImageBytes();
            if ( imageBytes != null ) {
                movieDetailsResponse.setImageBytes( Arrays.copyOf( imageBytes, imageBytes.length ) );
            }
        }

        updateMovieDetails( movieDetailsResponse, movie, userId );

        return movieDetailsResponse;
    }

    private String movieAddedByName(Movie movie) {
        if ( movie == null ) {
            return null;
        }
        User addedBy = movie.getAddedBy();
        if ( addedBy == null ) {
            return null;
        }
        String name = addedBy.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }
}
