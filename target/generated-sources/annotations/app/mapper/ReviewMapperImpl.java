package app.mapper;

import app.model.api.ReviewResponse;
import app.model.api.ReviewedUser;
import app.model.api.UserReviewData;
import app.model.database.Movie;
import app.model.database.Review;
import app.model.database.User;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-23T18:51:42+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.12 (Amazon.com Inc.)"
)
@Component
public class ReviewMapperImpl implements ReviewMapper {

    @Override
    public UserReviewData userReviewData(Review review) {
        if ( review == null ) {
            return null;
        }

        UserReviewData userReviewData = new UserReviewData();

        userReviewData.setMovieTitle( reviewMovieTitle( review ) );
        userReviewData.setId( review.getId() );
        userReviewData.setTitle( review.getTitle() );

        return userReviewData;
    }

    @Override
    public ReviewResponse reviewResponse(Review review) {
        if ( review == null ) {
            return null;
        }

        ReviewResponse reviewResponse = new ReviewResponse();

        reviewResponse.setMovieTitle( reviewMovieTitle( review ) );
        reviewResponse.setId( review.getId() );
        reviewResponse.setTitle( review.getTitle() );
        reviewResponse.setContent( review.getContent() );
        reviewResponse.setCreationDate( review.getCreationDate() );
        reviewResponse.setReviewedBy( userToReviewedUser( review.getReviewedBy() ) );

        updateReviewResponse( reviewResponse, review );

        return reviewResponse;
    }

    private String reviewMovieTitle(Review review) {
        if ( review == null ) {
            return null;
        }
        Movie movie = review.getMovie();
        if ( movie == null ) {
            return null;
        }
        String title = movie.getTitle();
        if ( title == null ) {
            return null;
        }
        return title;
    }

    protected ReviewedUser userToReviewedUser(User user) {
        if ( user == null ) {
            return null;
        }

        ReviewedUser reviewedUser = new ReviewedUser();

        reviewedUser.setId( user.getId() );
        reviewedUser.setName( user.getName() );

        return reviewedUser;
    }
}
