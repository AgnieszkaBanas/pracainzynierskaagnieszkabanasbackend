package app.mapper;

import app.model.api.ReviewComment;
import app.model.database.Comment;
import app.model.database.User;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-23T18:51:42+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.12 (Amazon.com Inc.)"
)
@Component
public class CommentMapperImpl implements CommentMapper {

    @Override
    public List<ReviewComment> reviewComments(List<Comment> comments) {
        if ( comments == null ) {
            return null;
        }

        List<ReviewComment> list = new ArrayList<ReviewComment>( comments.size() );
        for ( Comment comment : comments ) {
            list.add( reviewComment( comment ) );
        }

        return list;
    }

    @Override
    public ReviewComment reviewComment(Comment comment) {
        if ( comment == null ) {
            return null;
        }

        ReviewComment reviewComment = new ReviewComment();

        reviewComment.setAuthor( commentAuthorFirstName( comment ) );
        reviewComment.setContent( comment.getContent() );
        if ( comment.getCreationDate() != null ) {
            reviewComment.setCreationDate( DateTimeFormatter.ISO_LOCAL_DATE.format( comment.getCreationDate() ) );
        }

        return reviewComment;
    }

    private String commentAuthorFirstName(Comment comment) {
        if ( comment == null ) {
            return null;
        }
        User author = comment.getAuthor();
        if ( author == null ) {
            return null;
        }
        String firstName = author.getFirstName();
        if ( firstName == null ) {
            return null;
        }
        return firstName;
    }
}
