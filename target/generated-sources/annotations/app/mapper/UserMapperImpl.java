package app.mapper;

import app.model.api.InitialUserDataRequest;
import app.model.api.UserDataResponse;
import app.model.api.UserDetailsResponse;
import app.model.database.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-23T18:51:42+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.12 (Amazon.com Inc.)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public User updateUser(User user, InitialUserDataRequest input) {
        if ( input == null ) {
            return null;
        }

        user.setId( input.getId() );
        user.setDescription( input.getDescription() );
        user.setCountryWithCity( input.getCountryWithCity() );

        user.setFirstLogin( false );

        return user;
    }

    @Override
    public UserDetailsResponse userDetailsResponse(User user) {
        if ( user == null ) {
            return null;
        }

        UserDetailsResponse userDetailsResponse = new UserDetailsResponse();

        userDetailsResponse.setId( user.getId() );
        userDetailsResponse.setName( user.getName() );
        userDetailsResponse.setDescription( user.getDescription() );
        userDetailsResponse.setCountryWithCity( user.getCountryWithCity() );
        userDetailsResponse.setAccountCreationDate( user.getAccountCreationDate() );
        byte[] imageBytes = user.getImageBytes();
        if ( imageBytes != null ) {
            userDetailsResponse.setImageBytes( Arrays.copyOf( imageBytes, imageBytes.length ) );
        }
        userDetailsResponse.setIsReviewer( user.getIsReviewer() );

        updateUserDetailsResponse( userDetailsResponse, user );

        return userDetailsResponse;
    }

    @Override
    public List<UserDataResponse> followers(List<User> users) {
        if ( users == null ) {
            return null;
        }

        List<UserDataResponse> list = new ArrayList<UserDataResponse>( users.size() );
        for ( User user : users ) {
            list.add( userDataResponse( user ) );
        }

        return list;
    }

    @Override
    public UserDataResponse userDataResponse(User user) {
        if ( user == null ) {
            return null;
        }

        UserDataResponse userDataResponse = new UserDataResponse();

        userDataResponse.setId( user.getId() );
        userDataResponse.setName( user.getName() );
        byte[] imageBytes = user.getImageBytes();
        if ( imageBytes != null ) {
            userDataResponse.setImageBytes( Arrays.copyOf( imageBytes, imageBytes.length ) );
        }
        userDataResponse.setIsReviewer( user.getIsReviewer() );

        updateUserDataResponse( userDataResponse, user );

        return userDataResponse;
    }
}
