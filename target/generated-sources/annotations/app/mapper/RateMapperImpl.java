package app.mapper;

import app.model.api.UserMovieRate;
import app.model.database.Rate;
import app.model.database.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-23T18:51:43+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.12 (Amazon.com Inc.)"
)
@Component
public class RateMapperImpl implements RateMapper {

    @Override
    public List<UserMovieRate> usersMovieRate(List<Rate> rates) {
        if ( rates == null ) {
            return null;
        }

        List<UserMovieRate> list = new ArrayList<UserMovieRate>( rates.size() );
        for ( Rate rate : rates ) {
            list.add( userMovieRate( rate ) );
        }

        return list;
    }

    @Override
    public UserMovieRate userMovieRate(Rate rate) {
        if ( rate == null ) {
            return null;
        }

        UserMovieRate userMovieRate = new UserMovieRate();

        byte[] imageBytes1 = rateUserImageBytes( rate );
        if ( imageBytes1 != null ) {
            userMovieRate.setImageBytes( Arrays.copyOf( imageBytes1, imageBytes1.length ) );
        }
        userMovieRate.setName( rateUserName( rate ) );
        userMovieRate.setId( rateUserId( rate ) );
        userMovieRate.setValue( rate.getValue() );

        updateUserMovieRate( userMovieRate );

        return userMovieRate;
    }

    private byte[] rateUserImageBytes(Rate rate) {
        if ( rate == null ) {
            return null;
        }
        User user = rate.getUser();
        if ( user == null ) {
            return null;
        }
        byte[] imageBytes = user.getImageBytes();
        if ( imageBytes == null ) {
            return null;
        }
        return imageBytes;
    }

    private String rateUserName(Rate rate) {
        if ( rate == null ) {
            return null;
        }
        User user = rate.getUser();
        if ( user == null ) {
            return null;
        }
        String name = user.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    private Long rateUserId(Rate rate) {
        if ( rate == null ) {
            return null;
        }
        User user = rate.getUser();
        if ( user == null ) {
            return null;
        }
        Long id = user.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
